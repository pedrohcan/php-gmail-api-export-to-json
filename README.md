# 1. Configuração do GMAIL API

## 1.1. Habilitar a API e pegar credenciais
1. Acessar o site do [Gmail API](https://developers.google.com/gmail/api/quickstart/php) para habilitar o acesso
2. Clicar em "ENABLE GMAIL API"
3. Clicar em "DOWNLOAD CLIENT CONFIGURATION"
4. Salvar o arquivo no diretório do projeto como credentials.json

## 1.2. Instalação do Projeto
1. Clonar o repositório
2. Instalar o projeto com o [composer](https://getcomposer.org/) comando ```composer install```

## 1.3. Execução
1. Executar ```php bin/console app:gmail:download```
2. Abrir o link apresentado em tela no navegador
3. Copiar e inserir código de verificação no terminal
4. Aguardar a geração do arquivo
5. Ele será inserido na pasta ./result/

## 1.4. Observações
1. Os arquivos de credentials e tokens não são comitados e nem os de result, por motivos de: ninguém os e-mails por ai na mão dos outros.
2. Se quiser passar arquivos diferentes, use --credentials= e --token= na chamada da função com o caminho absoluto aos arquivos.

# 2. Preparação dos dados

## 2.1. Limpeza de dados
Com os dados categorizados, limpe-os e padronize-os com:
```
php bin/console app:email:clean --file='/var/www/gmail-integration/result/pedrohcan@gmail.com.json'
```

Note que usei o novo arquivo gerado no passo anterior.

Esta etapa irá criar um novo arquivo.

## 2.2. Categorização de dados
Após ter os dados em mãos, executar o script de categorização e informar o que é SPAM e o que não é:
```
php bin/console app:email:categorize --file='/var/www/gmail-integration/result/pedrohcan@gmail.com-clean.json'
```
Nesta parte basta apertar digitar "yes" para quando for SPAM e qualquer outra coisa, inclusive vazio quando não for.

Note que usei o novo arquivo gerado no passo anterior.

Esta etapa irá criar um novo arquivo.

## 2.3. Separação em conjunto de testes e conjunto de aprendizado
*Ainda não implementado.*

## 2.4. Criação de bag of words
Com os dados tratados conforme anteriormente, conseguimos ter o saco de palavras criados com o script abaixo:
```
php bin/console app:stopwords --file='/var/www/gmail-integration/result/pedrohcan@gmail.com-categorized-clean.json'
```

