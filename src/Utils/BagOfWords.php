<?php

namespace Utils;

class BagOfWords
{
    private $words = [];
    private $documents = [];
    private $wordsByDocument = [];


    public function add(string $word, ?string $document = null)
    {
        if (!isset($this->words[$word])) {
            $this->words[$word] = 1;
        } else {
            ++$this->words[$word];
        }

        if (!is_null($document)) {
            if (!isset($this->documents[$document])) {
                $this->documents[$document] = 1;
            } else {
                ++$this->documents[$document];
            }

            if (!isset($this->wordsByDocument[$word])) {
                $this->wordsByDocument[$word] = [];
            }

            if (!isset($this->wordsByDocument[$word][$document])) {
                $this->wordsByDocument[$word][$document] = 1;
            } else {
                ++$this->wordsByDocument[$word][$document];
            }
        }
    }

    public function sort(): BagOfWords
    {
        asort($this->words);

        return $this;
    }

    public function getWords(): array
    {
        return $this->words;
    }

    public function tf(string $word): float
    {
        return $this->words[$word] / array_sum($this->words);
    }

    public function idf(string $word): float
    {
        return log(count($this->documents) / count($this->wordsByDocument[$word]));
    }

    public function tfidf(string $word): float
    {
        return $this->tf($word) * $this->idf($word);
    }

    public function propability($word)
    {
        return ($this->words[$word] + 1) / (array_sum($this->words) + count($this->words) + 2);
    }
}
