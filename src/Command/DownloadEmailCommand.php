<?php

namespace Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Helper\ProgressBar;

class DownloadEmailCommand extends Command
{
    protected static $defaultName = 'app:gmail:download';

    protected function configure()
    {
        $this
            ->setDescription('Downloads the user emails from Gmail API and saves to JSON.')
            ->addOption('credentials', 'c', InputOption::VALUE_REQUIRED, 'Path to credentials file')
            ->addOption('token', 't', InputOption::VALUE_REQUIRED, 'Path to token file')
            ->addOption('maxmessages', 'm', InputOption::VALUE_REQUIRED, 'Number of messages to be retrieved (between 1 and 500)')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->input = $input;
        $this->output = $output;

        $output->writeln([
            'E-mail downloader',
            '=================',
            '',
        ]);

        $credentialsFile = $input->getOption('credentials');
        if (empty($credentialsFile)) {
            $credentialsFile = __PROJECTDIR__ . 'credentials.json';
        }

        $tokenFile = $input->getOption('token');
        if (empty($tokenFile)) {
            $tokenFile = __PROJECTDIR__ . 'token.json';
        }

        $maxMessages = $input->getOption('maxmessages');
        if (empty($maxMessages)) {
            $maxMessages = 500;
        }

        $client = $this->getClient($credentialsFile, $tokenFile);
        $service = $this->getService($client);

        $profile = $this->getProfile($service);

        $output->writeln("Fetching messages");
        $messages = $this->getMessages($service, $maxMessages);

        $progressBar = new ProgressBar($output, count($messages));
        $progressBar->setFormat('very_verbose');

        $progressBar->start();

        $list = [];
        foreach ($messages as $messageInfo) {
            $list[] = $this->format(
                $this->getMessage($service, $messageInfo->getId())
            );

            $progressBar->advance();
        }

        $progressBar->finish();

        $output->writeln("");
        $output->writeln("Saving to file");

        $filename = $profile->getEmailAddress() . '.json';

        (new \Symfony\Component\Filesystem\Filesystem())
            ->dumpFile(
                __PROJECTDIR__ . '/result/' . $filename,
                json_encode($list)
            )
        ;

        $output->writeln("Export ended");
    }

    private function getClient(string $credentialsFile, string $tokenFile): \Google_Client
    {
        $client = new \Google_Client();
        $client->setApplicationName('Gmail API PHP Quickstart');
        $client->setScopes(\Google_Service_Gmail::GMAIL_READONLY);
        $client->setAuthConfig($credentialsFile);
        $client->setAccessType('offline');
        $client->setPrompt('select_account consent');

        if (file_exists($tokenFile)) {
            $accessToken = json_decode(file_get_contents($tokenFile), true);
            $client->setAccessToken($accessToken);
        }

        // If there is no previous token or it's expired.
        if ($client->isAccessTokenExpired()) {
            // Refresh the token if possible, else fetch a new one.
            if ($client->getRefreshToken()) {
                $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
            } else {
                // Request authorization from the user.
                $authUrl = $client->createAuthUrl();
                $this->output->writeln([
                    "Open the following link in your browser:",
                    $authUrl
                ]);

                $question = new Question('Enter verification code:', null);
                $authCode = $helper->ask($this->input, $this->output, $question);

                // Exchange authorization code for an access token.
                $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
                $client->setAccessToken($accessToken);

                // Check to see if there was an error.
                if (array_key_exists('error', $accessToken)) {
                    throw new \Exception(join(', ', $accessToken));
                }
            }

            // Save the token to a file.
            if (!file_exists(dirname($tokenFile))) {
                mkdir(dirname($tokenFile), 0700, true);
            }
            file_put_contents($tokenFile, json_encode($client->getAccessToken()));
        }

        return $client;
    }

    private function getService(\Google_Client$client): \Google_Service_Gmail
    {
        return new \Google_Service_Gmail($client);
    }

    private function getProfile(\Google_Service_Gmail $service): \Google_Service_Gmail_Profile
    {
        $user = 'me';

        return $service->users->getProfile($user);
    }

    private function getMessages(\Google_Service_Gmail $service, $maxResults = 500): \Google_Service_Gmail_ListMessagesResponse
    {
        $user = 'me';

        return $service->users_messages->listUsersMessages($user, [
            'includeSpamTrash' => true,
            'maxResults' => $maxResults,
        ]);
    }

    private function getMessage(\Google_Service_Gmail $service, string $messageId): \Google_Service_Gmail_Message
    {
        $user = 'me';

        return $service->users_messages->get($user, $messageId);
    }

    private function format($message)
    {
        $labels = [];
        if (!empty($message->getLabelIds())) {
            foreach ($message->getLabelIds() as $labelId) {
                $labels[] = $labelId;
                $labelsAux[$labelId] = $labelId;
            }
        }

        $headers = $message->getPayload()->getHeaders();

        $subject = '';
        $date = '';
        foreach ($headers as $header) {
            if ($header->getName() == 'Subject') {
                $subject = $header->getValue();
            } elseif ($header->getName() == 'Date') {
                $date = $header->getValue();
            }
        }

        $content = $message->getPayload()->getBody()->getData();
        if (empty($content)) {
            $content = '';
            foreach ($message->getPayload()->getParts() as $part) {
                $content .= $this->convertBody($part->getBody()->getData()) . ' ';
            }
        } else {
            $content = $this->convertBody($content);
        }

        return [
            'id' => $message->getId(),
            'date' => (new \DateTime(trim(substr($date, 0, 31))))->format('Y-m-d H:i:s'),
            'subject' => $subject,
            'content' => $content,
            'labels' => $labels,
        ];
    }

    public function convertBody(?string $content): string
    {
        $content = str_replace(array('-', '_'), array('+', '/'), $content);
        $content = base64_decode($content);
        $content = \Soundasleep\Html2Text::convert($content, [
            'ignore_errors' => true,
            'drop_links' => true,
        ]);

        return $content;
    }
}
