<?php

namespace Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Finder\Finder;

class ClearEmailCommand extends Command
{
    protected static $defaultName = 'app:email:clear';

    protected function configure()
    {
        $this
            ->setDescription('Cleans the body of the e-mail for unwanted chars')
            ->addOption('file', 'c', InputOption::VALUE_REQUIRED, 'Path to e-mail json file')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->input = $input;
        $this->output = $output;

        $output->writeln([
            'E-mail cleaner',
            '==============',
            '',
        ]);

        $file = $input->getOption('file');
        if (empty($file)) {
            throw new \Exception('The file must be provided with a full path');
        }

        $content = file_get_contents($file);
        $messages = json_decode($content, true);

        $progressBar = new ProgressBar($output, count($messages));
        $progressBar->setFormat('very_verbose');
        $progressBar->start();

        foreach ($messages as $key => $message) {
            $message['content'] = $this->removeLinks($message['content']);
            $message['subject'] = $this->removeLinks($message['subject']);

            $messages[$key]['content'] = $this->clear($message['content']);
            $messages[$key]['subject'] = $this->clear($message['subject']);

            $progressBar->advance();
        }

        $progressBar->finish();

        $output->writeln("Saving to file");

        (new \Symfony\Component\Filesystem\Filesystem())
            ->dumpFile(
                str_replace('.json', '-clean.json', $file),
                json_encode($messages)
            )
        ;

        $output->writeln("Reading ended");
    }

    private function removeLinks(string $string): string
    {
        $regex = "/(?i)\b((?:https?:\/\/|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'\".,<>?«»“”‘’]))/";

        $urls = [];
        preg_match_all($regex, $string, $urls);

        foreach ($urls[0] as $url) {
            $urlParsed = parse_url($url);
            $host = $urlParsed['host'] ?? $urlParsed['path'];

            $string = str_replace($url, 'linkto ' . $host, $string);
        }

        return $string;
    }

    private function clear(string $string): string
    {
        $string = mb_strtolower($string);
        $string = preg_replace('/[^\p{L}0-9]/u', ' ', $string);
        $string = preg_replace('/\s+/', ' ', $string);
        $string = trim($string);

        return $string;
    }
}
