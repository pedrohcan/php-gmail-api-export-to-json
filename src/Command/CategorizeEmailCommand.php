<?php

namespace Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Finder\Finder;

class CategorizeEmailCommand extends Command
{
    protected static $defaultName = 'app:email:categorize';

    protected function configure()
    {
        $this
            ->setDescription('Allows the user to read the JSON data and categorize it as INBOX or SPAM.')
            ->addOption('file', 'c', InputOption::VALUE_REQUIRED, 'Path to e-mail json file')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->input = $input;
        $this->output = $output;

        $output->writeln([
            'E-mail categorizing',
            '=================',
            '',
        ]);

        $file = $input->getOption('file');
        if (empty($file)) {
            throw new \Exception('The file must be provided with a full path');
        }

        $sectionProgressBar = $output->section();
        $progressBar = new ProgressBar($sectionProgressBar);
        $progressBar->setFormat('very_verbose');

        $helper = $this->getHelper('question');
        $sectionQuestions = $output->section();

        $question = new ConfirmationQuestion('Is this a SPAM? (yes or y to confirm / anything else for no )', false);

        $content = file_get_contents($file);
        $messages = json_decode($content, true);

        $progressBar->start(count($messages));

        foreach ($messages as $key => $message) {
            $progressBar->advance();

            $sectionQuestions->writeln([
                '-----------------',
                'Dados da mensagem',
                '-----------------',
                'Data: ' . $message['date'],
                'Assunto: ' . $message['subject'],
                'Conteúdo: ',
                $message['content'],
                '-----------------',
                '',
            ]);

            if ($helper->ask($input, $sectionQuestions, $question)) {
                $category = 'SPAM';
            } else {
                $category = 'INBOX';
            }

            $sectionQuestions->clear();

            $messages[$key]['category'] = $category;
        }

        $progressBar->finish();

        $output->writeln("Saving to file");

        (new \Symfony\Component\Filesystem\Filesystem())
            ->dumpFile(
                str_replace('.json', '-categorized.json', $file),
                json_encode($messages)
            )
        ;

        $output->writeln("Reading ended");
    }
}
