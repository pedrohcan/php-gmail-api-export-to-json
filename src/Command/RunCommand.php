<?php

namespace Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Finder\Finder;

class RunCommand extends Command
{
    protected static $defaultName = 'app:run';

    protected function configure()
    {
        $this
            ->setDescription('Checks if a email is a SPAM based os training a naive bayes classifier')
            ->addOption('file', 'c', InputOption::VALUE_REQUIRED, 'Path to e-mail json file')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->input = $input;
        $this->output = $output;

        $output->writeln([
            'Shazam! :)',
            '============',
            '',
        ]);

        $file = $input->getOption('file');
        if (empty($file)) {
            throw new \Exception('The file must be provided with a full path');
        }

        $messages = json_decode(file_get_contents($file), true);

        // TODO: make the set random
        $sets = $this->getRandomSet($messages, 20);// $sets['training'], $sets['test']

        $progressBar = new ProgressBar($output, count($sets['training']));
        $progressBar->setFormat('very_verbose');
        $progressBar->start();

        $bagsOfWords = [
            'content' => new \Utils\BagOfWords(),
            'subject' => new \Utils\BagOfWords(),
            'full' => new \Utils\BagOfWords(),
        ];

        // Gerando Sacolas de palavras
        foreach ($sets['training'] as $key => $message) {
            $words = explode(' ', $message['content']);
            foreach ($words as $word) {
                $bagsOfWords['content']->add($word, $message['id']);
            }

            $words = explode(' ', $message['subject']);
            foreach ($words as $word) {
                $bagsOfWords['subject']->add($word, $message['id']);
            }

            $words = explode(' ', $message['content'] . ' ' . $message['subject']);
            foreach ($words as $word) {
                $bagsOfWords['full']->add($word, $message['id']);
            }

            $progressBar->advance();
        }

        $progressBar->finish();


        // Somente debug e prints em tela para ver resultados parciais
        $tfidf = [];
        foreach ($bagsOfWords['full']->getWords() as $word => $quantity) {
            $idf[$word] = $bagsOfWords['full']->idf($word);
            $tf[$word] = $bagsOfWords['full']->tf($word);
            $tfidf[$word] = $bagsOfWords['full']->tfidf($word);
        }

        arsort($tfidf);

        $text = '';
        foreach ($tfidf as $word => $value) {
            $text .= $word . "\t" . $value . "\t" . $tf[$word] . "\t" . $idf[$word] . "\t" . $bagsOfWords['full']->getWords()[$word] . "\n";
        }

        echo $text;
    }

    private function getRandomSet(array $list, int $threshold = 20): array
    {
        $limit = (count($list) * $threshold) / 100;

        return [
            'test' => array_slice($list, 0, $limit),
            'training' => array_slice($list, $limit, count($list)),
        ];
    }
}
